package photoarchiverjava.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.Date;

public class ArchiverMetadata {
    private Date date;
    private ArrayList<String> keywords;
    private String name;
    private String desc;

    public ArchiverMetadata(Date date, ArrayList<String> keywords, String name, String desc) {
        this.date = date;
        this.keywords = keywords;
        this.name = name;
        this.desc = desc;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ArrayList<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(ArrayList<String> keywords) {
        this.keywords = keywords;
    }
    
    public void addKeyword(String keyword){
        keywords.add(keyword);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return "MyMetaData{" + "date=" + date + ", keywords=" + keywords + ", name=" + name + ", desc=" + desc + '}';
    }    
}
