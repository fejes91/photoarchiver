/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package photoarchiverjava.entity;

import java.awt.image.BufferedImage;
import java.io.File;

/**
 *
 * @author adam
 */
public class ArchiverImage {
    private File file;
    private BufferedImage thumbnail;
    private ArchiverMetadata meta;

    public ArchiverImage(File file, ArchiverMetadata meta) {
        this.file = file;
        this.meta = meta;
    }

    public File getFile() {
        return file;
    }

    public ArchiverMetadata getMeta() {
        return meta;
    }

    public BufferedImage getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(BufferedImage thumbnail) {
        this.thumbnail = thumbnail;
    }
    
}
