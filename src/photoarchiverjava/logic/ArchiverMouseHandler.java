package photoarchiverjava.logic;

import java.awt.Component;
import photoarchiverjava.ui.PAFrame;
import photoarchiverjava.ui.ThumbnailPanel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.beans.PropertyVetoException;
import java.text.Collator;
import java.text.RuleBasedCollator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JSlider;
import javax.swing.SwingUtilities;
import photoarchiverjava.entity.ArchiverMetadata;
import photoarchiverjava.util.ColorPalette;
import photoarchiverjava.util.Utils;

public class ArchiverMouseHandler extends MouseAdapter {

    private PAFrame frame;

    public ArchiverMouseHandler(PAFrame frame) {
        this.frame = frame;
    }

    @Override
    public void mousePressed(MouseEvent e) {
        //Utils.saveMetadata(frame.getEditedImageKeywords().getText(), frame.getDatePicker().getDate());
        //System.out.println("class: " + e.getSource().getClass());
        if (e.getSource() instanceof ThumbnailPanel) {
            ThumbnailPanel panel = (ThumbnailPanel) e.getSource();
            
            //panel.requestFocus();
            int clickedIndex = PAFrame.thumbnailPanels.indexOf(panel);
//        System.out.println("BEFORE CLICK HANDLING: Min index: " + PAFrame.minSelectedIndex + "   max index: " + PAFrame.maxSelectedIndex);
//        System.out.println("Clicked index: " + clickedIndex);
            if (e.isControlDown()) {
                toggleSelection(panel);
            } else if (e.isShiftDown()) {
//            System.out.println("Shift click on index " + clickedIndex);
                int loopStart, loopEnd;

                if (clickedIndex < PAFrame.minSelectedIndex) {
                    loopStart = clickedIndex;
                    loopEnd = PAFrame.maxSelectedIndex;
                } else {
                    loopStart = PAFrame.maxSelectedIndex;
                    loopEnd = clickedIndex;
                }

                for (int i = loopStart; i <= loopEnd; i++) {
                    ThumbnailPanel p = PAFrame.thumbnailPanels.get(i);
                    selectPanel(p);
                }
            } 
            else {
                //no keystroke
                deselectAllPanel();
                selectPanel(panel);
            }
        } 
        else {
            System.out.println("mellé kattint");
            deselectAllPanel();
            //Utils.saveMetadata(frame.getEditedImageKeywords().getText(), frame.getDatePicker().getDate());
        }
//        System.out.println("AFTER CLICK HANDLING: Min index: " + PAFrame.minSelectedIndex + "   max index: " + PAFrame.maxSelectedIndex);
        if (PAFrame.selectedThumbnailPanels.isEmpty()) {
            frame.setEditorTitle("");
            frame.getEditorPanel().hideElements();
            try {
                //frame.getEditedImageDate().setText("");
                frame.getDatePicker().setDate(null);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(ArchiverMouseHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
            frame.getEditedImageKeywords().setText("");
        } else if (PAFrame.selectedThumbnailPanels.size() == 1) {
            ThumbnailPanel selectedPanel = (ThumbnailPanel) PAFrame.selectedThumbnailPanels.iterator().next();
            selectedPanel.mouseEnteredAction();
            ArchiverMetadata meta = selectedPanel.getMetaData();
            frame.setEditorTitle(meta.getName());
            frame.getEditorPanel().showElements();
            try {
                frame.getDatePicker().setDate(meta.getDate());
            } catch (PropertyVetoException ex) {
                Logger.getLogger(ArchiverMouseHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                frame.getDatePicker().setDate(meta.getDate());
            } catch (PropertyVetoException ex) {
                Logger.getLogger(ArchiverMouseHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
            String keywords = "";
            System.out.println("load " + meta.getKeywords().size() + " keyword(s)");
            for (String keyword : meta.getKeywords()) {
                if (!keyword.isEmpty() && !keyword.equals("$")) {
                    keywords += keyword + " ";
                }
            };
            frame.getEditedImageKeywords().setText(keywords.trim());
        } else if (PAFrame.selectedThumbnailPanels.size() > 1) {
            frame.setEditorTitle("Selected " + PAFrame.selectedThumbnailPanels.size() + " photos");
            //frame.getEditorPanel().showElements();
            //frame.getDatePicker().setVisible(false);
            try {
                //frame.getEditedImageDate().setText("*");
                frame.getDatePicker().setDate(null);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(ArchiverMouseHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
            RuleBasedCollator myCollator = (RuleBasedCollator) Collator.getInstance(new Locale("hu", "HU"));
            Map<String, Integer> keywords = new TreeMap<>(myCollator);
            for (ThumbnailPanel thumbnailPanel : PAFrame.selectedThumbnailPanels) {
                thumbnailPanel.mouseExitedAction();
                ArchiverMetadata meta = thumbnailPanel.getMetaData();
                for (String keyword : meta.getKeywords()) {
                    if (keywords.containsKey(keyword)) {
                        keywords.put(keyword, keywords.get(keyword) + 1);
                    } else {
                        keywords.put(keyword, 1);
                    }
                }
            }

            StringBuilder keywordsStr = new StringBuilder();
            for (String keyword : keywords.keySet()) {
                if (!keyword.isEmpty() && !keyword.equals("$")) {
                    keywordsStr.append(keyword);
                    if (keywords.get(keyword) < PAFrame.selectedThumbnailPanels.size()) {
                        keywordsStr.append("*");
                    }
                    keywordsStr.append(" ");
                }
            }

            frame.getEditedImageKeywords().setText(keywordsStr.toString().trim());
        }

        frame.getEditorPanel().updateUI();
        frame.getEditorPanel().refreshPanelVisiblities();
    }

    private void toggleSelection(ThumbnailPanel panel) {
        if (PAFrame.selectedThumbnailPanels.contains(panel)) {
            deselectPanel(panel);
        } else {
            selectPanel(panel);
        }
    }

    private void selectPanel(ThumbnailPanel panel) {
        PAFrame.selectedThumbnailPanels.add(panel);
        panel.setBackground(ColorPalette.SELECTED_THUMBNAIL_BACKGROUND);

        int index = PAFrame.thumbnailPanels.indexOf(panel);
        if (index <= PAFrame.minSelectedIndex) {
            PAFrame.minSelectedIndex = index;
        }
        if (index >= PAFrame.maxSelectedIndex) {
            PAFrame.maxSelectedIndex = index;
        }
    }

    private void deselectPanel(ThumbnailPanel panel) {
        Utils.saveMetadata(frame.getEditedImageKeywords().getText(), frame.getDatePicker().getDate());
        PAFrame.selectedThumbnailPanels.remove(panel);
        panel.setBackground(ColorPalette.THUMBNAIL_BACKGROUND);

        int index = PAFrame.thumbnailPanels.indexOf(panel);
        if (index == PAFrame.minSelectedIndex) {
            PAFrame.minSelectedIndex = Integer.MAX_VALUE;
        }
        if (index == PAFrame.maxSelectedIndex) {
            PAFrame.maxSelectedIndex = -1;
        }
    }

    public void deselectAllPanel() {
//        System.out.println("deselect all panel");
        Utils.saveMetadata(frame.getEditedImageKeywords().getText(), frame.getDatePicker().getDate());
        Iterator it = PAFrame.selectedThumbnailPanels.iterator();
        while (it.hasNext()) {
            ThumbnailPanel panel = (ThumbnailPanel) it.next();
            it.remove();
            panel.setBackground(ColorPalette.THUMBNAIL_BACKGROUND);
        }
        PAFrame.minSelectedIndex = Integer.MAX_VALUE;
        PAFrame.maxSelectedIndex = -1;

//        System.out.println("AFTER DESELECT ALL: Min index: " + PAFrame.minSelectedIndex + "   max index: " + PAFrame.maxSelectedIndex);
    }
    
    public void selectAllPanel(){
        deselectAllPanel();
        for(ThumbnailPanel panel : PAFrame.thumbnailPanels){
            PAFrame.selectedThumbnailPanels.add(panel);
            panel.setBackground(ColorPalette.SELECTED_THUMBNAIL_BACKGROUND);
        }
        PAFrame.minSelectedIndex = 0;
        PAFrame.maxSelectedIndex = PAFrame.selectedThumbnailPanels.size() - 1;
        frame.getEditorPanel().showElements();
    }

    private void repaintSelections() {
        for (ThumbnailPanel panel : PAFrame.thumbnailPanels) {
            if (PAFrame.selectedThumbnailPanels.contains(panel)) {
                panel.setBackground(ColorPalette.SELECTED_THUMBNAIL_BACKGROUND);
            } else {
                panel.setBackground(ColorPalette.THUMBNAIL_BACKGROUND);
            }
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        //System.out.println("class: " + e.getSource().getClass());
        if (e.getSource() instanceof ThumbnailPanel) {
            ThumbnailPanel panel = (ThumbnailPanel) e.getSource();
            panel.mouseEnteredAction();
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        if (e.getSource() instanceof ThumbnailPanel) {
            ThumbnailPanel panel = (ThumbnailPanel) e.getSource();
            panel.mouseExitedAction();
        }
    }

    
    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        //System.out.println("scroll: " + e.getSource().getClass().getName());
        
        if (e.isControlDown()) {
            JSlider slider = frame.getZoomSlider();
            slider.setValue(slider.getValue() + e.getWheelRotation());
        } 
        else {
            frame.getThumbnailScrollPane().getScrollPane().dispatchEvent(e); //ha nem nyomja a ctrl-t akkor küldd az eventet tovább a scrollpane-nek, mert amúgy árnyékolja
        }
    }
}
