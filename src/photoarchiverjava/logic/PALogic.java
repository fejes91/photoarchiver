/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package photoarchiverjava.logic;

import photoarchiverjava.entity.ArchiverMetadata;
import photoarchiverjava.entity.ArchiverImage;

import photoarchiverjava.threads.ArchiverThread;
import photoarchiverjava.ui.PAFrame;
import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.Collator;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.RuleBasedCollator;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.imgscalr.Scalr;
import photoarchiverjava.Main;
import photoarchiverjava.util.Utils;

/**
 *
 * @author adam
 */
public class PALogic {

    private final String NO_DATA = "FILE HAS NOT METADATA!";
    private File vault;
    //private ArrayList<File> files = new ArrayList<>();
    //private ArrayList<ArchiverImage> inboxImages;
    private final PAFrame frame;
    private ArchiverThread archiver;
    private PrintStream printStream;

    public PALogic() {
        frame = new PAFrame(this);
    }

    public void doArchive() {
        archiver = new ArchiverThread(this);

        //printStream = new PrintStream(new CustomOutputStream(frame.getVaultInfo()));
        //writer = new SummaryWriterThread(summary, printStream);
        //writer.start();

        archiver.start();
    }

    private void storeMetadata(String path, String fileName, ArchiverMetadata md) {
        File metaDir = new File(vault.getAbsolutePath() + "/meta");
        metaDir.mkdir();
        if (md.getKeywords().isEmpty()) {
            md.addKeyword("$");
        }
        String allKeyword = "";
        for (String keyword : md.getKeywords()) {
            allKeyword += keyword + " ";
        }
        allKeyword = allKeyword.trim();

        for (String keyword : md.getKeywords()) {
            try {
                File keywordFile = new File(metaDir.getAbsolutePath() + "/" + keyword.hashCode() + ".js");
                if (keywordFile.createNewFile()) {//ha nem létezett, akkor fel kell venni a listába is
                    File listDir = new File(metaDir.getAbsolutePath() + "/list");
                    listDir.mkdir();
                    File keywordList = new File(metaDir.getAbsolutePath() + "/" + listDir.getName() + "/keywordlist.js");
                    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(keywordList, true)));
                    out.println("keywordlist.push(\"" + keyword + "\");");
                    out.flush();

                    File dictDir = new File(metaDir.getAbsolutePath() + "/list");
                    dictDir.mkdir();
                    File dictFile = new File(metaDir.getAbsolutePath() + "/" + dictDir.getName() + "/" + getDictionaryFileNameForKeyword(keyword) + ".js");
                    out = new PrintWriter(new BufferedWriter(new FileWriter(dictFile, true)));
                    out.println("addToDictionary(\"" + keyword + "\",\"" + keyword.hashCode() + "\");");
                    out.flush();
                }
                PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(keywordFile, true)));
                out.println("addPicture(\"" + path + "/" + fileName.hashCode() + ".jpg\", \"" + allKeyword + "\");");
                out.flush();
            } catch (Exception ex) {
                System.err.println(ex.getMessage());
            }
            //System.out.println("insert meta for file " + path + " to keyword file " + keywordFile.getName());
        }

        try {
            File folderDescriptorFile = new File(vault.getAbsolutePath() + "/" + path + "/desc.js");
            folderDescriptorFile.createNewFile();
            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(folderDescriptorFile, true)));
            out.println("addPicture(\"" + path + "/" + fileName.hashCode() + ".jpg\", \"" + allKeyword + "\");");
            out.flush();
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public void writeArchiveDate() {
        FileWriter fw = null;
        try {
            File metaDir = new File(vault.getAbsolutePath() + "/meta");
            metaDir.mkdir();
            File dateDir = new File(metaDir.getAbsolutePath() + "/dates");
            dateDir.mkdir();
            File dateList = new File(metaDir.getAbsolutePath() + "/" + dateDir.getName() + "/archiveDate.js");
            Calendar today = new GregorianCalendar();
            fw = new FileWriter(dateList.getAbsoluteFile());
            try (BufferedWriter bw = new BufferedWriter(fw)) {
                bw.write("archiveDate(\""
                        + Utils.newDateFormat.format(new Date()) + "\");");
            }
        } catch (IOException ex) {
            Logger.getLogger(PALogic.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fw.close();
            } catch (IOException ex) {
                Logger.getLogger(PALogic.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public ArchiverMetadata getMetadata(File f) {
        String name = f.getName();
        Date date = new Date();
        String desc = "";
        ArrayList<String> keywords = new ArrayList<>();
        Path path = Paths.get(f.getAbsolutePath());
        BasicFileAttributes attributes;
        try {
            attributes = Files.readAttributes(path, BasicFileAttributes.class);
            FileTime time = attributes.creationTime();
            date.setTime(time.toMillis());
        } catch (IOException ex) {
            System.out.println("\tFile attribute reading error: " + ex.getMessage());
        }

        Metadata metadata = null;
        try {
            metadata = ImageMetadataReader.readMetadata(f);
        } catch (ImageProcessingException | IOException ex) {
            System.err.println("Error on reading metadata: " + f.getName() + "\t" + ex);
        }

        if (metadata != null) {
            for (Directory directory : metadata.getDirectories()) {
                if (directory.getName().toLowerCase().equals("iptc")) {
                    for (Tag tag : directory.getTags()) {
                        if (tag.getTagName().toLowerCase().equals("keywords")) {
                            String temp = tag.getDescription();
                            if (temp != null) {
                                for (String keyword : Arrays.asList(temp.split(";"))) {
                                    keywords.add(keyword.replaceAll(" ", "_"));
                                }
                            }
                        }
                    }
                }
                if (directory.getName().toLowerCase().equals("exif subifd")) {
                    for (Tag tag : directory.getTags()) {
                        if (tag.getTagName().toLowerCase().equals("date/time original")) {
                            
                            try {
                                date = (Utils.imageMetaSourceDateFormat.parse(tag.getDescription()));
                            } catch (ParseException ex) {
                                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                }
            }
        }

        RuleBasedCollator myCollator = (RuleBasedCollator) Collator.getInstance(new Locale("hu", "HU")); //TODO konfigolható legyen
        Collections.sort(keywords, myCollator);
        return new ArchiverMetadata(date, keywords, name, desc);
    }

    public Boolean handleFile(ArchiverImage image) {
        File file = image.getFile();
        ArchiverMetadata md = image.getMeta();
        System.out.println("Handling file: " + file.getName());
        vault.mkdir();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(md.getDate());
        File year = new File(vault.getAbsolutePath() + "/" + calendar.get(Calendar.YEAR));
        year.mkdir();
        File month = new File(year.getAbsolutePath() + "/" + String.valueOf(calendar.get(Calendar.MONTH) + 1));
        month.mkdir();
        File day = new File(month.getAbsolutePath() + "/" + calendar.get(Calendar.DAY_OF_MONTH));
        if (!day.exists()) {
            PrintWriter out = null;
            try {
                File metaDir = new File(vault.getAbsolutePath() + "/meta");
                metaDir.mkdir();
                File dateDir = new File(metaDir.getAbsolutePath() + "/dates");
                dateDir.mkdir();
                File dateList = new File(metaDir.getAbsolutePath() + "/" + dateDir.getName() + "/dateList.js");
                out = new PrintWriter(new BufferedWriter(new FileWriter(dateList, true)));
                out.println("addDate(\"" + calendar.get(Calendar.YEAR) + "-" + (calendar.get(Calendar.MONTH) + 1) + "-" + calendar.get(Calendar.DAY_OF_MONTH) + "\");");
                out.flush();
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                out.close();
            }
        }
        day.mkdir();

        File thumbnails = new File(vault.getAbsolutePath() + "/thumbnails");
        thumbnails.mkdir();
        File tYear = new File(thumbnails.getAbsolutePath() + "/" + calendar.get(Calendar.YEAR));
        tYear.mkdir();
        File tMonth = new File(tYear.getAbsolutePath() + "/" + String.valueOf(calendar.get(Calendar.MONTH) + 1));
        tMonth.mkdir();
        File tDay = new File(tMonth.getAbsolutePath() + "/" + calendar.get(Calendar.DAY_OF_MONTH));
        tDay.mkdir();

        try {
            String time = String.valueOf(System.currentTimeMillis());
            Files.copy(Paths.get(file.getAbsolutePath()), Paths.get(day.getAbsolutePath() + "/" + (file.getName() + time).hashCode() + ".jpg"));
            BufferedImage thumbnail = image.getThumbnail();
            ImageIO.write(thumbnail, "jpg", new File(tDay.getAbsolutePath() + "/" + (file.getName() + time).hashCode() + ".jpg"));
            storeMetadata(year.getName() + "/" + month.getName() + "/" + day.getName(), file.getName() + time, md);
        } catch (FileAlreadyExistsException ex) {
            System.err.println("\tFile already exists: " + ex.getMessage());
            return false;
        } catch (IOException ex) {
            System.err.println("File moving error: " + ex.getMessage());
            return false;
        }
        return true;
    }

    public static BufferedImage resizeImage(BufferedImage originalImage, int w, int h, Scalr.Mode mode) {
        return Scalr.resize(originalImage, Scalr.Method.QUALITY, mode, w, h);
    }

    public void generateArchiveBrowser() {
        try {
            InputStream s = this.getClass().getResourceAsStream("/resources/archive_browser.html");

            BufferedReader br = new BufferedReader(new InputStreamReader(s));
            StringBuilder sb = new StringBuilder();

            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line + "\n");
            }

            File file = new File(vault.getAbsolutePath() + "/archive_browser.html");
            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(sb.toString());
            bw.close();

            System.out.println("Done");


        } catch (IOException ex) {
            Logger.getLogger(PALogic.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private String getAllMetadataAsString(File f) {
        String str = "";
        Metadata metadata = null;
        try {
            metadata = ImageMetadataReader.readMetadata(f);
        } catch (ImageProcessingException | IOException ex) {
            System.err.println("Metadata reading error: " + ex.getMessage() + "\t" + ex.getClass());
        }
        str += "Filename: " + f.getName() + "\n";
        if (metadata != null) {
            for (Directory directory : metadata.getDirectories()) {
                for (Tag tag : directory.getTags()) {
                    str += "\t" + tag + "\n";
                }
            }
        }
        str += "\n----------------------------------------------------------------------";
        return str;
    }

    private String getDictionaryFileNameForKeyword(String keyword) {
        String first = String.valueOf(keyword.charAt(0)).toLowerCase();
        if (first.equals("ö")) {
            return "o";
        } else if (first.equals("ü")) {
            return "u";
        } else if (first.equals("ó")) {
            return "o";
        } else if (first.equals("ő")) {
            return "o";
        } else if (first.equals("ú")) {
            return "u";
        } else if (first.equals("é")) {
            return "e";
        } else if (first.equals("á")) {
            return "a";
        } else if (first.equals("ű")) {
            return "u";
        } else if (first.equals("í")) {
            return "i";
        }
        return first;
    }

    public File getVault() {
        return vault;
    }

    public void setVault(File vault) {
        this.vault = vault;
    }


    public PAFrame getFrame() {
        return frame;
    }
}
