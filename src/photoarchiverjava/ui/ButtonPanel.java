package photoarchiverjava.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import photoarchiverjava.logic.PALogic;
import photoarchiverjava.util.ColorPalette;

/**
 *
 * @author adam
 */
public class ButtonPanel extends ArchiverPanel {

    private ArchiverPanel leftWrapper;
    private JLabel vaultLabel;
    private LabelButton setVaultBtn, setInboxBtn, doArchiveBtn;
    private JFileChooser dirChooser;
    public static final int PREFERRED_HEIGHT = 50;

    public ButtonPanel(final PALogic logic, final PAFrame frame) {
        this.setLayout(new BorderLayout());
        this.setBorder(BorderFactory.createMatteBorder(0, 0, PAFrame.BORDER_WIDTH, 0, ColorPalette.PANEL_BORDER));
        this.setPreferredSize(new Dimension(frame.getWidth(), PREFERRED_HEIGHT));
        setInboxBtn = new LabelButton("Add files", new ImageIcon(this.getClass().getResource("/resources/inbox.png")));
        setInboxBtn.setHorizontalTextPosition(SwingConstants.RIGHT);
        setInboxBtn.setHorizontalAlignment(SwingConstants.LEFT);
        setInboxBtn.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        setInboxBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                addToInboxAction(logic, frame);
            }
        });
        
        setVaultBtn = new LabelButton("Vault not set!", new ImageIcon(this.getClass().getResource("/resources/vault.png")));
        setVaultBtn.setHorizontalTextPosition(SwingConstants.LEFT);
        setVaultBtn.setHorizontalAlignment(SwingConstants.RIGHT);
        setVaultBtn.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        
        setVaultBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                setVaultAction(logic, frame);
            }
        });

        doArchiveBtn = new LabelButton("Archive!", new ImageIcon(this.getClass().getResource("/resources/archive.png")));
        doArchiveBtn.setHorizontalTextPosition(SwingConstants.RIGHT);
        doArchiveBtn.setHorizontalAlignment(SwingConstants.LEFT);
        doArchiveBtn.setBorder(BorderFactory.createEmptyBorder(5, 25, 5, 25));
        doArchiveBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                doArchiveAction(logic, frame);
            }
        });
        
        vaultLabel = new JLabel();
        vaultLabel.setHorizontalAlignment(SwingConstants.RIGHT);
                
        leftWrapper = new ArchiverPanel(new BorderLayout());
        leftWrapper.add(setInboxBtn, BorderLayout.WEST);
        leftWrapper.add(doArchiveBtn, BorderLayout.EAST);
        this.add(leftWrapper, BorderLayout.WEST);
        this.add(vaultLabel, BorderLayout.CENTER);
        this.add(setVaultBtn, BorderLayout.EAST);
    }

    private void addToInboxAction(PALogic logic, PAFrame parent) {
        if(!parent.isLoading()){
            parent.getMouseAdapter().deselectAllPanel();
            dirChooser = new JFileChooser(new File("/home/adam/Desktop/Inbox"));
            dirChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            dirChooser.setMultiSelectionEnabled(true);
            dirChooser.setDialogTitle("Choose files to add");
            dirChooser.showOpenDialog(parent);
            ArrayList<File> newFiles = new ArrayList<>(Arrays.asList(dirChooser.getSelectedFiles()));
            if (!newFiles.isEmpty()) {
                //inboxPath.setText("Inbox dir: " + logic.getInbox().getAbsolutePath());
                parent.loadPreviews(logic, parent.getImagesPanel(), newFiles);
            }
        }
    }

    private void setVaultAction(PALogic logic, PAFrame parent) {
        parent.getMouseAdapter().deselectAllPanel();
        dirChooser = new JFileChooser(new File("."));
        dirChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        dirChooser.setDialogTitle("Choose vault directory");
        dirChooser.showOpenDialog(parent);
        logic.setVault(dirChooser.getSelectedFile());
        if (logic.getVault() != null) {
            setVaultBtn.setText("<html>Vault dir: " + logic.getVault().getAbsolutePath()/* + "<br> 123 photos in vault</html>"*/);
            //TODO vault tároljon magáról adatot amit ide be tudunk olvasni (pl hány kép van benne)
            //setVaultBtn.setText("");
        }
    }

    private void doArchiveAction(PALogic logic, PAFrame parent) {
        parent.getMouseAdapter().deselectAllPanel();
        System.out.println("Vault: " + logic.getVault());
        System.out.println("Inbox: " + PAFrame.thumbnailPanels.size());
        System.out.println("Is loading: " + parent.isLoading());
        if (!parent.isLoading() && logic.getVault() != null && PAFrame.thumbnailPanels != null) {
            logic.doArchive();
        }
    }
}
