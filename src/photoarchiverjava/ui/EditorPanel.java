package photoarchiverjava.ui;

import java.awt.Dimension;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JTextArea;

public class EditorPanel extends ArchiverPanel {

    private ArchiverDatePicker datePicker;
    private JTextArea editedImageKeywords, editedImageTitle;

    public EditorPanel() {
        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        this.setBorder(BorderFactory.createEmptyBorder(10, 20, 10, 20));
        editedImageTitle = new JTextArea();
        editedImageTitle.setMinimumSize(new Dimension(0, 75));
        editedImageTitle.setFont(new Font("Arial", Font.PLAIN, 14));
        editedImageTitle.setLineWrap(true);
        editedImageTitle.setOpaque(false);
        
        datePicker = new ArchiverDatePicker();
        datePicker.addFocusListener(new PAFocusListener(PAFocusListener.DATE, datePicker));
        add(datePicker);

        editedImageKeywords = new JTextArea("editedImageKeywords");
        editedImageKeywords.setMinimumSize(new Dimension(0, 200));
        editedImageKeywords.addFocusListener(new PAFocusListener(PAFocusListener.KEYWORDS, editedImageKeywords));
        editedImageKeywords.setLineWrap(true);
        editedImageKeywords.setWrapStyleWord(true);

        this.add(editedImageTitle);
        this.add(Box.createRigidArea(new Dimension(0, 30)));
        this.add(datePicker);
        this.add(Box.createRigidArea(new Dimension(0, 30)));
        this.add(editedImageKeywords);
        this.add(Box.createRigidArea(new Dimension(0, 30000)));
        this.hideElements();
    }

    public void refreshPanelVisiblities(){
        System.out.println("refreshPanelVisiblities - " + PAFrame.selectedThumbnailPanels.size());
        if(PAFrame.selectedThumbnailPanels.isEmpty()){
            hideElements();
        }
        else if(PAFrame.selectedThumbnailPanels.size() > 1){
            editedImageTitle.setVisible(false);
            datePicker.setVisible(false);
            editedImageKeywords.setVisible(true);
        }
        else{
            showElements();
        }
    }
    
    public ArchiverDatePicker getDatePicker(){
        return datePicker;
    }

    public JTextArea getEditedImageKeywords() {
        return editedImageKeywords;
    }

    public void setTitle(String title){
        String ending = "";
        if(title.length() > 100){
            ending = "...";
        }
        editedImageTitle.setText(title.substring(0, Math.min(99, title.length())) + ending);
    }
    
    public void showElements(){
        editedImageKeywords.setVisible(true);
        editedImageTitle.setVisible(true);
        datePicker.setVisible(true);
    }
    
    public final void hideElements(){
        editedImageKeywords.setVisible(false);
        editedImageTitle.setVisible(false);
        datePicker.setVisible(false);
    }
}
