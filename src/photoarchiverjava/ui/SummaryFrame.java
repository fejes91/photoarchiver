/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package photoarchiverjava.ui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import photoarchiverjava.util.ColorPalette;

/**
 *
 * @author adam
 */
public class SummaryFrame extends JFrame{

    public SummaryFrame(final JFrame parent, String msg){
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setAlwaysOnTop(true);
        this.setAutoRequestFocus(true);
        this.setUndecorated(true);
        this.setTitle("Archiving done!");
        this.setSize(300, 200);
        this.setLocationRelativeTo(parent);
        
        ArchiverPanel mainPanel = new ArchiverPanel(new GridLayout(4, 1));
        mainPanel.setBorder(BorderFactory.createLineBorder(ColorPalette.PANEL_BORDER));
        
        JLabel label = new JLabel(msg);
        label.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        
        LabelButton closeButton = new LabelButton("OK");
        final SummaryFrame frame = this;
        closeButton.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                parent.setEnabled(true);
                frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
            }
        });

        mainPanel.add(new ArchiverPanel());
        mainPanel.add(label);
        mainPanel.add(new ArchiverPanel());
        mainPanel.add(closeButton);
        
        add(mainPanel);
        this.setVisible(true);
    }
}
