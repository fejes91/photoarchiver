/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package photoarchiverjava.ui;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;

/**
 *
 * @author adam
 */
public class ArchiverProgressBar extends JProgressBar {

    @Override
    public void setValue(int n) {
        super.setValue(n);
        if (n == getMaximum()) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(3000);
                        setVisible(false);
                        setValue(getMinimum());
                    } catch (InterruptedException ex) {
                        Logger.getLogger(ArchiverProgressBar.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });


        }
    }
}
