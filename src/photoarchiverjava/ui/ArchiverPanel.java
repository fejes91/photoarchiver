package photoarchiverjava.ui;

import java.awt.Component;
import java.awt.LayoutManager;
import javax.swing.JPanel;
import photoarchiverjava.util.ColorPalette;

/**
 *
 * @author adam
 */
public class ArchiverPanel extends JPanel{

    public ArchiverPanel() {
        super();
        initLooks();
    }

    public ArchiverPanel(LayoutManager layout, boolean isDoubleBuffered) {
        super(layout, isDoubleBuffered);
        initLooks();
    }

    public ArchiverPanel(LayoutManager layout) {
        super(layout);
        initLooks();
    }

    public ArchiverPanel(boolean isDoubleBuffered) {
        super(isDoubleBuffered);
        initLooks();
    }

    private void initLooks(){
        setBackground(ColorPalette.PANEL_BACKGROUND);
    }

}
