package photoarchiverjava.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Objects;
import javax.swing.JPanel;
import org.imgscalr.Scalr;
import photoarchiverjava.entity.ArchiverImage;
import photoarchiverjava.entity.ArchiverMetadata;
import photoarchiverjava.logic.PALogic;
import photoarchiverjava.util.ColorPalette;

public class ThumbnailPanel extends JPanel {

    private ArchiverImage image;
    private BufferedImage resizedThumb;
    private int topLeftX;
    private int topLeftY;
    private int bottomRightX;
    private int bottomRightY;
    private int thumbnailWidth;
    private int thumbnailHeight;
    private JPanel buttonContainer;
    private LabelButton removeButton;
    private boolean needToResize;

    public ThumbnailPanel(Dimension d, ArchiverImage image, final PAFrame frame) {
        this.image = image;
        setupSizes(d);
        this.setBackground(ColorPalette.THUMBNAIL_BACKGROUND);
        setLayout(new BorderLayout());

        needToResize = false;

        buttonContainer = new JPanel(new BorderLayout());
        buttonContainer.setOpaque(false);

        removeButton = new LabelButton("X", new Dimension(30, 30));
        removeButton.setVisible(false);
        removeButton.addMouseListener(new RemoveButtonMouseHandler(this));
        final ThumbnailPanel thisPanel = this;
        removeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.removePanel(thisPanel);
            }
        });

        buttonContainer.add(removeButton, BorderLayout.EAST);

        add(buttonContainer, BorderLayout.NORTH);
    }

    private void setupSizes(Dimension d) {
        //setMinimumSize(d);
        setSize(d);
        //setPreferredSize(d);
        //setMaximumSize(d);
    }

    public ArchiverMetadata getMetaData() {
        return image.getMeta();
    }

    public BufferedImage getThumbnail() {
        return image.getThumbnail();
    }

    public ArchiverImage getImage() {
        return image;
    }

    public File getFile() {
        return image.getFile();
    }

    public void loadImage(BufferedImage thumbnail) {
        image.setThumbnail(thumbnail);
    }

    public void resizePanel(Dimension d) {
        needToResize = true;
        //System.out.println("Resize panel to " + d);
        setupSizes(d);
        //System.out.println("actual size: " + getSize());
        //this.repaint();
    }

    public void setImage(BufferedImage thumbnail) {
        image.setThumbnail(thumbnail);
        needToResize = true;
        this.repaint();
    }

    private Scalr.Mode getScalingMode() {
        if ((double) getThumbnail().getWidth() / (double) getThumbnail().getHeight() > (double) getSize().width / (double) getSize().height) {
            return Scalr.Mode.FIT_TO_WIDTH;
        }

        return Scalr.Mode.FIT_TO_HEIGHT;
    }

    @Override
    protected void paintComponent(final Graphics g) {
        //System.out.println("paint component " + image.getMeta().getName());
        super.paintComponent(g);
        final Dimension actualSize = getSize();
        if (getThumbnail() != null) {
            if (resizedThumb == null || needToResize) {
                needToResize = false;
                System.out.println("resizing " + image.getMeta().getName());
                System.out.println("Generate thumbnail: " + (int) ((double) actualSize.width * 0.90) + "x" + (int) ((double) actualSize.height * 0.90));
                resizedThumb = PALogic.resizeImage(getThumbnail(), (int) ((double) actualSize.width * 0.90), (int) ((double) actualSize.height * 0.90), getScalingMode());
            }
            thumbnailWidth = resizedThumb.getWidth();
            thumbnailHeight = resizedThumb.getHeight();


            topLeftX = (actualSize.width - thumbnailWidth) / 2;
            topLeftY = (actualSize.height - thumbnailHeight) / 2;
            bottomRightX = topLeftX + thumbnailWidth;
            bottomRightY = topLeftY + thumbnailHeight;
            g.drawImage(resizedThumb, topLeftX, topLeftY, bottomRightX, bottomRightY, 0, 0, thumbnailWidth, thumbnailHeight, null);
        }
    }

    public void mouseEnteredAction() {
        if (PAFrame.selectedThumbnailPanels.isEmpty() || PAFrame.selectedThumbnailPanels.size() == 1 && ((ThumbnailPanel) PAFrame.selectedThumbnailPanels.iterator().next()).equals(this)) {
            removeButton.setVisible(true);
        }
    }

    public void mouseExitedAction() {
        removeButton.setVisible(false);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.image.getFile());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ThumbnailPanel other = (ThumbnailPanel) obj;
        if (!Objects.equals(this.image.getFile(), other.image.getFile())) {
            return false;
        }
        return true;
    }

    private class RemoveButtonMouseHandler extends MouseAdapter {

        private final ThumbnailPanel panel;

        public RemoveButtonMouseHandler(ThumbnailPanel panel) {
            this.panel = panel;
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            panel.mouseEnteredAction();
        }

        @Override
        public void mouseExited(MouseEvent e) {
            panel.mouseExitedAction();
        }
    }
}