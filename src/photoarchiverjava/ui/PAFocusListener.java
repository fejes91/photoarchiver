/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package photoarchiverjava.ui;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.JComponent;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import photoarchiverjava.util.Utils;

/**
 *
 * @author adam
 */
public class PAFocusListener implements FocusListener{
    public final static int DATE = 0;
    public final static int KEYWORDS = 1;
    
    private final int type;
    private JComponent component;
    
    public PAFocusListener(int type, JComponent component) {
        this.type = type;
        this.component = component;
    }
    
    
    @Override
    public void focusGained(FocusEvent e) {
        
    }

    @Override
    public void focusLost(FocusEvent e) {
        System.out.println("lost focus on " + component.getName() + " (" + type + ")");
        if(type == KEYWORDS){
            Utils.saveMetadata(((JTextArea)component).getText(), null);
        }
        else{
            Utils.saveMetadata(null, ((ArchiverDatePicker)component).getDate());
        }
        
    }
    
}
