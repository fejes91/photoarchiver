/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package photoarchiverjava.ui;

import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

/**
 *
 * @author adam
 */
public class PAFrameListener implements ComponentListener{
    
    PAFrame frame;

    public PAFrameListener(PAFrame frame) {
        this.frame = frame;
    }
    
    
        
    @Override
    public void componentResized(ComponentEvent e) {
        frame.resizeComponents();
    }

    @Override
    public void componentMoved(ComponentEvent e) {
    }

    @Override
    public void componentShown(ComponentEvent e) {
        
    }

    @Override
    public void componentHidden(ComponentEvent e) {
        
    }
    
}
