package photoarchiverjava.ui;

import java.awt.Graphics;
import java.awt.event.ActionListener;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;
import photoarchiverjava.util.ColorPalette;

/**
 *
 * @author adam
 */
public class ArchiverButton extends JButton {

    public ArchiverButton() {
        initLooks();
    }

    public ArchiverButton(Icon icon) {
        super(icon);
        initLooks();
    }

    public ArchiverButton(String text) {
        super(text);
        initLooks();
    }

    public ArchiverButton(Action a) {
        super(a);
        initLooks();
    }

    public ArchiverButton(String text, Icon icon) {
        super(text, icon);
        initLooks();
    }

    private void initLooks() {
        setBackground(null);
        setFocusPainted(false);
        setContentAreaFilled(false);
    }
    
    @Override
    protected void paintComponent(Graphics g) {
        if (getModel().isPressed()) {
            g.setColor(ColorPalette.BUTTON_CLICK);
        } else if (getModel().isRollover()) {
            g.setColor(ColorPalette.BUTTON_HOVER);
        } else {
            g.setColor(getBackground());
        }
        g.fillRect(0, 0, getWidth(), getHeight());
        super.paintComponent(g);
    }

}
