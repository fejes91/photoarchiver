package photoarchiverjava.ui;

import com.michaelbaranov.microba.calendar.CalendarPane;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import photoarchiverjava.util.ColorPalette;

public class ArchiverDatePicker extends CalendarPane {

    public ArchiverDatePicker() {
        super();
        
        // this.setDateFormat(new SimpleDateFormat("HH dd yyyy"));
        //this.setDateFormat(Utils.newDateFormat);

        this.setEnabled(true);
        //this.setKeepTime(true);
        this.setStripTime(false);
        this.setShowNumberOfWeek(false);
        this.setShowTodayButton(false);
        setShowNoneButton(false);
        setLocale(Locale.UK);
        setStyle(STYLE_CLASSIC);
        
        Map ov = new HashMap();
        ov.put(CalendarPane.COLOR_CALENDAR_GRID_FOREGROUND_ENABLED, Color.black);
        ov.put(CalendarPane.COLOR_CALENDAR_HEADER_BACKGROUND_ENABLED, ColorPalette.CALENDAR_HEADER);
        ov.put(CalendarPane.COLOR_CALENDAR_GRID_FOCUS, ColorPalette.CALENDAR_HEADER);
        ov.put(CalendarPane.COLOR_CALENDAR_GRID_SELECTION_BACKGROUND_ENABLED, ColorPalette.CALENDAR_HEADER);
        this.setColorOverrideMap(ov);
        
        
        this.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //System.out.println("DatePicker:" + getDate());

            }
        });
    }
}
