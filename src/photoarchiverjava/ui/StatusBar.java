/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package photoarchiverjava.ui;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.GeneralPath;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicSliderUI;
import photoarchiverjava.util.ColorPalette;

/**
 *
 * @author adam
 */
public class StatusBar extends ArchiverPanel {

    private JLabel inboxStatus;
    private ArchiverProgressBar progressBar;
    private LabelButton removeSelected;
    private ArchiverPanel rightContainer, leftContainer;
    private JSlider zoomSlider;
    private static final int preferredHeight = 30;

    public StatusBar(final PAFrame frame) {
        super(new BorderLayout());
        this.setBorder(BorderFactory.createMatteBorder(PAFrame.BORDER_WIDTH, 0, 0, 0, ColorPalette.PANEL_BORDER));
        this.setPreferredSize(new Dimension(frame.getWidth(), preferredHeight));

        removeSelected = new LabelButton("Remove selected");
        removeSelected.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.removeSelectedPanels();
            }
        });

        zoomSlider = new JSlider(JSlider.HORIZONTAL, 2, 10, PAFrame.thumbnailPerRow);
        zoomSlider.setMinorTickSpacing(1);
        zoomSlider.setPaintTicks(true);
        zoomSlider.setPaintLabels(true);
        zoomSlider.setOpaque(false);
        zoomSlider.setUI(new ZoomSliderUI(zoomSlider));
        zoomSlider.setPreferredSize(new Dimension(100, 0));
        zoomSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                JSlider source = (JSlider) e.getSource();
                if (!source.getValueIsAdjusting()) {
                    PAFrame.thumbnailPerRow = source.getValue();
                    frame.getThumbnailScrollPane().setColumns(PAFrame.thumbnailPerRow);
                    System.out.println("Slider: " + PAFrame.thumbnailPerRow);
                    frame.resizeComponents();
                    frame.getImagesPanel().revalidate();
                }
            }
        });

        leftContainer = new ArchiverPanel(new BorderLayout());
        leftContainer.add(removeSelected, BorderLayout.EAST);
        leftContainer.add(zoomSlider, BorderLayout.WEST);

        UIManager.put("ProgressBar.selectionForeground", Color.white);
        UIManager.put("ProgressBar.selectionBackground", Color.black);
        progressBar = new ArchiverProgressBar();
        progressBar.setStringPainted(true);
        progressBar.setValue(0);
        progressBar.setVisible(false);
        progressBar.setBackground(Color.WHITE);
        progressBar.setForeground(ColorPalette.SCROLL_BAR_COLOR);
        progressBar.setBorderPainted(false);

        inboxStatus = new JLabel();
        inboxStatus.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));

        rightContainer = new ArchiverPanel(new BorderLayout());
        rightContainer.add(progressBar, BorderLayout.WEST);
        rightContainer.add(inboxStatus, BorderLayout.EAST);


        this.add(leftContainer, BorderLayout.WEST);
        this.add(rightContainer, BorderLayout.EAST);
    }

    public JLabel getInboxStatus() {
        return inboxStatus;
    }

    public ArchiverProgressBar getProgressBar() {
        return progressBar;
    }

    public JSlider getZoomSlider() {
        return zoomSlider;
    }

    private class ZoomSliderUI extends BasicSliderUI {
        public ZoomSliderUI(JSlider b) {
            super(b);
        }

        @Override
        public void paintTrack(Graphics g) {
            Graphics2D g2d = (Graphics2D) g;
            Stroke old = g2d.getStroke();
            g2d.setStroke(old);
            g2d.setPaint(Color.BLACK);
            if (slider.getOrientation() == SwingConstants.HORIZONTAL) {
                g2d.drawLine(trackRect.x, trackRect.y + trackRect.height / 2,
                        trackRect.x + trackRect.width, trackRect.y + trackRect.height / 2);
            } else {
                g2d.drawLine(trackRect.x + trackRect.width / 2, trackRect.y,
                        trackRect.x + trackRect.width / 2, trackRect.y + trackRect.height);
            }
            g2d.setStroke(old);
        }

        @Override
        public void paintThumb(Graphics g) {
            Graphics2D g2d = (Graphics2D) g;
            int x1 = thumbRect.x + 2;
            int x2 = thumbRect.x + thumbRect.width - 2;
            int width = thumbRect.width - 4;
            int topY = thumbRect.y + thumbRect.height / 2 - thumbRect.width / 3;
            GeneralPath shape = new GeneralPath(GeneralPath.WIND_EVEN_ODD);
            shape.moveTo(x1, topY);
            shape.lineTo(x2, topY);
            shape.lineTo((x1 + x2) / 2, topY + width);
            shape.closePath();
            g2d.setPaint(ColorPalette.SCROLL_BAR_COLOR);
            g2d.fill(shape);
            Stroke old = g2d.getStroke();
            g2d.setStroke(new BasicStroke(2f));
            g2d.setPaint(ColorPalette.SCROLL_BAR_COLOR);
            g2d.draw(shape);
            g2d.setStroke(old);
        }
    }
}
