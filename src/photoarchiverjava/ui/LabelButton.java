package photoarchiverjava.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import photoarchiverjava.util.ColorPalette;

/**
 *
 * @author adam
 */
public class LabelButton extends ArchiverButton {

    public LabelButton(Dimension d) {
        init(d);
    }

    public LabelButton(ImageIcon icon) {
        Image image = icon.getImage();
        Image resizedImage = image.getScaledInstance(ButtonPanel.PREFERRED_HEIGHT - 5, ButtonPanel.PREFERRED_HEIGHT - 5, Image.SCALE_SMOOTH);
        this.setIcon(new ImageIcon(resizedImage));
    }

    public LabelButton(String text, Dimension d) {
        super(text);
        init(d);
    }
    
    public LabelButton(String text) {
        super(text);
        init();
    }

    public LabelButton(Action a, Dimension d) {
        super(a);
        init(d);
    }

    public LabelButton(String text, ImageIcon icon, Dimension d) {
        super(text);
        Image image = icon.getImage();
        Image resizedImage = image.getScaledInstance(ButtonPanel.PREFERRED_HEIGHT - 5, ButtonPanel.PREFERRED_HEIGHT - 5, Image.SCALE_SMOOTH);
        this.setIcon(new ImageIcon(resizedImage));
        init(d);
    }

    public LabelButton(String text, ImageIcon icon) {
        super(text);
        Image image = icon.getImage();
        Image resizedImage = image.getScaledInstance(ButtonPanel.PREFERRED_HEIGHT - 5, ButtonPanel.PREFERRED_HEIGHT - 5, Image.SCALE_SMOOTH);
        this.setIcon(new ImageIcon(resizedImage));
        init();
    }

    private void init(Dimension d) {
        setPreferredSize(d);
        init();
    }

    private void init() {
        setFont(new Font("Arial", Font.PLAIN, 14));
        setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    }

    
}
