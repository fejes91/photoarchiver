package photoarchiverjava.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.plaf.OptionPaneUI;
import photoarchiverjava.entity.ArchiverImage;
import photoarchiverjava.logic.ArchiverMouseHandler;
import photoarchiverjava.logic.PALogic;
import photoarchiverjava.threads.FileLoaderWorker;

public class PAFrame extends JFrame {

    private JPanel buttonPanel, mainPanel;
    private EditorPanel editorPanel;
    private StatusBar statusBar;
    private ThumbnailScrollPane thumbnailScrollPane;
    
    public static ArrayList<ThumbnailPanel> thumbnailPanels = new ArrayList<>();
    public static final HashSet<ThumbnailPanel> selectedThumbnailPanels = new HashSet<>();
    public static int minSelectedIndex, maxSelectedIndex;
    
    public static final int BORDER_WIDTH = 1;
    public static int thumbnailPerRow = 3;

    public static long thumbnailLoadStarted;
    public static int thumbnailsLoaded;
    
    private ArchiverMouseHandler mouseAdapter;
    
    public PAFrame(final PALogic logic) {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                createAndShowGui(logic);
            }
        });
        
    }

    private void createAndShowGui(final PALogic logic) {
        
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("PhotoArchiver 0.2");
        this.setSize(1200, 800);
        this.setLocationRelativeTo(null);
        this.addComponentListener(new PAFrameListener(this));
        mouseAdapter = new ArchiverMouseHandler(this);
        addMouseListener(mouseAdapter);
        buttonPanel = new ButtonPanel(logic, this);

        mainPanel = new JPanel(new BorderLayout());
        editorPanel = new EditorPanel();
        mainPanel.add(editorPanel, BorderLayout.EAST);

        thumbnailScrollPane = new ThumbnailScrollPane(mouseAdapter);
        mainPanel.addMouseListener(mouseAdapter);

        mainPanel.add(thumbnailScrollPane, BorderLayout.CENTER);

        statusBar = new StatusBar(this);

        resizeComponents();

        this.add(buttonPanel, BorderLayout.NORTH);
        this.add(mainPanel, BorderLayout.CENTER);
        this.add(statusBar, BorderLayout.SOUTH);
        this.setVisible(true);
        
        KeyStroke key = KeyStroke.getKeyStroke(KeyEvent.VK_A, KeyEvent.CTRL_DOWN_MASK);
        mainPanel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(key, "selectAll");
        mainPanel.getActionMap().put("selectAll", new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {
                selectAllThumbnails();
            }
        });
    }

    public void resizeComponents() {
        System.out.println("Resize components");
        editorPanel.setPreferredSize(new Dimension(300, mainPanel.getHeight()));

        Dimension thumbnailDimension = getThumbnailDimension();
        for (ThumbnailPanel panel : thumbnailPanels) {
            panel.resizePanel(thumbnailDimension);
            panel.mouseExitedAction();
        }
        thumbnailScrollPane.resizeWrapper(getBlockHeight());
    }

    public void loadPreviews(PALogic logic, JPanel imagesPanel, ArrayList<File> newFiles) {
        Dimension panelDimension = getThumbnailDimension();
        ArrayList<ThumbnailPanel> newPanels = new ArrayList();
        for (File f : newFiles) {
            ThumbnailPanel thumbnailPanel = new ThumbnailPanel(panelDimension, new ArchiverImage(f, logic.getMetadata(f)), this);
            if (!thumbnailPanels.contains(thumbnailPanel)) {                
                thumbnailPanels.add(thumbnailPanel);
                updateInboxStatus();
                thumbnailPanel.addMouseListener(mouseAdapter);
                imagesPanel.add(thumbnailPanel);
                newPanels.add(thumbnailPanel);
            } else {
                System.out.println("File " + f.getName() + " already loaded");
            }
        }
        
        thumbnailScrollPane.resizeWrapper(getBlockHeight());
        imagesPanel.updateUI();

        getProgressBar().setMaximum(newFiles.size());

        thumbnailLoadStarted = System.currentTimeMillis();

        FileLoaderWorker threadWrapper = new FileLoaderWorker(newPanels, getProgressBar());
        threadWrapper.execute();

    }
    
    public void showArchiveSummary(int success, int error){
        setEnabled(false);
        StringBuilder str = new StringBuilder();
        str.append("<html>");
        str.append(success);
        str.append(" image successfully archived");
        if(error > 0){
            str.append("<br>");
            str.append(error);
            str.append(" error occured");
        }
        str.append("</html>");
        new SummaryFrame(this, str.toString());
    }
    
    public void removePanel(ThumbnailPanel panel){
        thumbnailPanels.remove(panel);
        getImagesPanel().remove(panel);
        thumbnailScrollPane.resizeWrapper(getBlockHeight());
        updateInboxStatus();
        editorPanel.hideElements();
        getImagesPanel().updateUI();
    }
    
    public void removeSelectedPanels(){
        Iterator it = selectedThumbnailPanels.iterator();
        while(it.hasNext()){
            removePanel((ThumbnailPanel)it.next());
        }
    }
    
    public int getBlockHeight(){
        double numberOfRows = Math.ceil((double)getThumbnailCount() / (double)thumbnailPerRow);
        System.out.println("Numberofrows: " + numberOfRows);
        return (int)numberOfRows * getThumbnailDimension().height;
    }

    private Dimension getThumbnailDimension() {
        int paneWidth = thumbnailScrollPane.getWidth();
        if (paneWidth > 0) {
            System.out.println("pane width: " + paneWidth);

            double thumbnailWidth = (int) Math.floor((double)paneWidth / (double)thumbnailPerRow) - ThumbnailScrollPane.THUMBNAIL_GAP;
            return new Dimension((int)thumbnailWidth, (int)(thumbnailWidth / 3.0 * 2.0));
        }
        return new Dimension();
    }

    public static int getThumbnailCount() {
        if (PAFrame.thumbnailPanels != null) {
            return PAFrame.thumbnailPanels.size();
        }
        return 0;
    }

    public EditorPanel getEditorPanel() {
        return editorPanel;
    }

    public void setEditorTitle(String title) {
        editorPanel.setTitle(title);
    }
    
    public boolean isLoading(){
        return thumbnailsLoaded < thumbnailPanels.size();
    }

//    public JTextField getEditedImageDate() {
//        return editorPanel.getEditedImageDate();
//    }
    
    public ArchiverDatePicker getDatePicker(){
        return editorPanel.getDatePicker();
    }

    public JTextArea getEditedImageKeywords() {
        return editorPanel.getEditedImageKeywords();
    }

    public JPanel getImagesPanel() {
        return thumbnailScrollPane.getImagesPanel();
    }

    public ArchiverProgressBar getProgressBar() {
        return statusBar.getProgressBar();
    }

    public JLabel getInboxStatusLabel() {
        return statusBar.getInboxStatus();
    }

    public ArchiverMouseHandler getMouseAdapter() {
        return mouseAdapter;
    }

    public ThumbnailScrollPane getThumbnailScrollPane() {
        return thumbnailScrollPane;
    }
    
    private void updateInboxStatus(){
        getInboxStatusLabel().setText(getThumbnailCount() + " photos loaded");
    }
    
    public JSlider getZoomSlider(){
        return statusBar.getZoomSlider();
    }
    
    public void selectAllThumbnails(){
        mouseAdapter.selectAllPanel();
    }
}
