package photoarchiverjava.threads;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.plaf.basic.BasicProgressBarUI;
import org.imgscalr.Scalr;
import photoarchiverjava.logic.PALogic;
import photoarchiverjava.ui.ArchiverProgressBar;
import photoarchiverjava.ui.PAFrame;
import photoarchiverjava.ui.ThumbnailPanel;

/**
 *
 * @author adam
 */
public class FileLoaderWorker extends SwingWorker<Integer, Integer>{
    List<ThumbnailPanel> panels;
    private ArchiverProgressBar progressBar;
    private final static int NORMAL_THUMBAIL_WIDTH = 600;
    private final static int NORMAL_THUMBAIL__HEIGHT = 400;
    private int progress = 0;

    public FileLoaderWorker(List<ThumbnailPanel> panels, ArchiverProgressBar pb) {
        this.panels = panels;
        this.progressBar = pb;
        progressBar.setMinimum(0);
        progressBar.setMaximum(panels.size());
        progressBar.setVisible(true);
    }

    @Override
    protected Integer doInBackground() throws Exception {
        for(final ThumbnailPanel panel : panels){
            try {
                final BufferedImage image = ImageIO.read(panel.getFile());
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        panel.setImage(PALogic.resizeImage(image, NORMAL_THUMBAIL_WIDTH, NORMAL_THUMBAIL__HEIGHT, Scalr.Mode.AUTOMATIC));
                        publish(++progress);
                        ++PAFrame.thumbnailsLoaded;
                    }
                });
            } catch (IOException ex) {
                System.err.println("File loading error on " + panel.getFile().getName() + "\n" + ex);
            }
        }
        return 1;
    }

    @Override
    protected void process(List<Integer> chunks) {
        progressBar.setValue(chunks.get(chunks.size() - 1));
    }

    @Override
    protected void done() {
        System.out.println("Swingworker done in " + (new Date().getTime() - PAFrame.thumbnailLoadStarted) / 1000 + " seconds");
        super.done();
        
    }
}
