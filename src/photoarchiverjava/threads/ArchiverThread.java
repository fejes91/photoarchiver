/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package photoarchiverjava.threads;


import javax.swing.JOptionPane;
import photoarchiverjava.logic.PALogic;
import photoarchiverjava.ui.PAFrame;
import photoarchiverjava.ui.ThumbnailPanel;

/**
 *
 * @author adam
 */
public class ArchiverThread extends Thread{
    PALogic logic;
    
    public ArchiverThread(PALogic l) {
        logic = l;
    }
    
    
    @Override
    public void run(){              
        logic.generateArchiveBrowser();
        
        int success = 0;
        int error = 0;
        
        for(ThumbnailPanel panel : PAFrame.thumbnailPanels){        
            if(logic.handleFile(panel.getImage())){
                ++success;
            }
            else{
                ++error;
            }
        }
        
        logic.writeArchiveDate();
        logic.getFrame().showArchiveSummary(success, error);
    }
    
}
