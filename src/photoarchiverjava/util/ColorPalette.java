package photoarchiverjava.util;

import java.awt.Color;

/**
 *
 * @author adam
 */
public class ColorPalette {
    public static final Color BUTTON_CLICK = new Color(157, 157, 157);
    public static final Color BUTTON_HOVER = new Color(224, 224, 224);
    
    public static final Color CALENDAR_HEADER = new Color(224, 224, 224);
    
    public static final Color PANEL_BACKGROUND = new Color(250, 250, 250);
    public static final Color THUMBNAIL_BACKGROUND = new Color(224, 224, 224);
    public static final Color SELECTED_THUMBNAIL_BACKGROUND = new Color(157, 157, 157);
    
    public static final Color PANEL_BORDER = new Color(224, 224, 224);
    
    public static final Color SCROLL_BAR_COLOR = Color.GRAY;
}
