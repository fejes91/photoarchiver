/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package photoarchiverjava.util;

import java.text.Collator;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.RuleBasedCollator;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import photoarchiverjava.Main;
import photoarchiverjava.entity.ArchiverMetadata;
import photoarchiverjava.ui.ThumbnailPanel;
import photoarchiverjava.ui.PAFrame;

/**
 *
 * @author adam
 */
public class Utils {
    
    public static final DateFormat newDateFormat = new SimpleDateFormat("yyyy. MM. dd. HH:mm:ss", Locale.ENGLISH);
    public static final DateFormat imageMetaSourceDateFormat = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss", Locale.ENGLISH);

    public static void saveMetadata(String keywordStr, Date date) {
        if (!PAFrame.selectedThumbnailPanels.isEmpty()) {
            ArrayList<String> unsortedKeywordsList = null;
            if(keywordStr != null){
                    String[] keywords = keywordStr.trim().split(" ");
                    unsortedKeywordsList = new ArrayList<>(Arrays.asList(keywords));
            }
            if (PAFrame.selectedThumbnailPanels.size() == 1) {
                ArchiverMetadata meta = PAFrame.selectedThumbnailPanels.iterator().next().getMetaData();
                if(unsortedKeywordsList != null){
                    RuleBasedCollator myCollator = (RuleBasedCollator) Collator.getInstance(new Locale("hu", "HU"));
                    Collections.sort(unsortedKeywordsList, myCollator);
                    meta.setKeywords(unsortedKeywordsList);
                    System.out.println("save " + meta.getKeywords().size() + " keyword(s)");
                }
                if(date != null){
                    System.out.println("save date: " + date);
                    meta.setDate(date);
                }
            } 
            else {
                for(ThumbnailPanel panel : PAFrame.selectedThumbnailPanels){
                    ArchiverMetadata meta = panel.getMetaData();
                    ArrayList<String> currentKeywords = new ArrayList<>();
                    if(unsortedKeywordsList != null){
                        for(String keyword : unsortedKeywordsList){
                            if(keyword.endsWith("*")){
                                String realKeyword = keyword.replaceAll("\\*", "");
                                if(meta.getKeywords().contains(realKeyword)){
                                    currentKeywords.add(realKeyword);
                                }
                            }
                            else{
                                currentKeywords.add(keyword);
                            }
                        }
                        RuleBasedCollator myCollator = (RuleBasedCollator) Collator.getInstance(new Locale("hu", "HU"));
                        Collections.sort(currentKeywords, myCollator);
                        meta.setKeywords(currentKeywords);
                        System.out.println("save " + currentKeywords.size() + " keyword(s)");
                    }
//                    if(date != null){
//                        meta.setDate(date);
//                    }
                    //TODO batch date setting
                }
            }
        }
    }
    
    private static Calendar getDateFromStr(String dateStr){
        if(dateStr.equals("*")){
            return null;
        }
        Calendar date = new GregorianCalendar();
        
        try {
            date.setTime(newDateFormat.parse(dateStr));
            return date;
        } catch (ParseException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
