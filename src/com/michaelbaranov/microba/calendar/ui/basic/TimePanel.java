/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.michaelbaranov.microba.calendar.ui.basic;

import com.michaelbaranov.microba.calendar.CalendarPane;
import static com.michaelbaranov.microba.calendar.ui.basic.ModernCalendarPanel.PROPERTY_NAME_DATE;
import static com.michaelbaranov.microba.calendar.ui.basic.ModernCalendarPanel.PROPERTY_NAME_ZONE;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicSpinnerUI;
import photoarchiverjava.ui.ArchiverButton;
import photoarchiverjava.ui.LabelButton;
import photoarchiverjava.util.ColorPalette;

/**
 *
 * @author adam
 */
public class TimePanel extends JPanel implements ChangeListener {

    JSpinner time;
    SpinnerDateModel dateModel;
    JSpinner.DateEditor timeEditor;
    Date date;
    Locale locale;
    TimeZone zone;
    private Set focusableComponents = new HashSet();

    public TimePanel(CalendarPane peer, Date date, Locale locale, TimeZone zone) {
        super();

        setLayout(new BorderLayout());
        setBorder(BorderFactory.createEmptyBorder(3, 80, 3, 80));
        setMinimumSize(new Dimension(300, 35));
        setBackground(ColorPalette.CALENDAR_HEADER);

        dateModel = new SpinnerDateModel();
        time = new JSpinner(dateModel);
        timeEditor = new JSpinner.DateEditor(time, "HH:mm:ss");
        time.setEditor(timeEditor);
        time.setPreferredSize(new Dimension(200, 35));
        time.addChangeListener(this);

        focusableComponents.add(time);

        add(time, BorderLayout.CENTER);

        this.date = date;
        this.locale = locale;
        this.zone = zone;
        reflectData();
    }

    private void reflectData() {
        time.setValue(date);
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        Date oldDate = this.date;
        this.date = date;
        firePropertyChange(PROPERTY_NAME_DATE, oldDate, date);
        time.setValue(date == null ? new Date() : date);
    }

    public TimeZone getZone() {
        return zone;
    }

    public void setZone(TimeZone zone) {
        TimeZone old = this.zone;
        this.zone = zone;
        firePropertyChange(PROPERTY_NAME_ZONE, old, zone);
    }

    public Set getFocusableComponents() {
        return focusableComponents;
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        Calendar dateCal = Calendar.getInstance();
        dateCal.setTime(date == null ? new Date() : date);
        //System.out.println("Full date: " + dateCal.getTime());

        Calendar timeCal = Calendar.getInstance();
        timeCal.setTime(dateModel.getDate());
        //System.out.println("Timecal: " + timeCal.getTime());
        dateCal.set(Calendar.HOUR_OF_DAY, timeCal.get(Calendar.HOUR_OF_DAY));
        dateCal.set(Calendar.MINUTE, timeCal.get(Calendar.MINUTE));
        dateCal.set(Calendar.SECOND, timeCal.get(Calendar.SECOND));

        setDate(dateCal.getTime());
    }
}
